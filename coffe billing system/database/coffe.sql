-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 12, 2019 at 11:37 AM
-- Server version: 5.1.53
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coffe`
--

-- --------------------------------------------------------

--
-- Table structure for table `basket_tbl`
--

CREATE TABLE IF NOT EXISTS `basket_tbl` (
  `basket_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(20) NOT NULL,
  `item_id` int(10) NOT NULL,
  `dt` datetime NOT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`basket_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `basket_tbl`
--

INSERT INTO `basket_tbl` (`basket_id`, `user_email`, `item_id`, `dt`, `qty`) VALUES
(19, 'rp240796@gmail.com', 2, '2019-12-08 03:58:15', 9),
(18, 'rp240796@gmail.com', 1, '2019-12-08 03:57:57', 2);

-- --------------------------------------------------------

--
-- Table structure for table `item_tbl`
--

CREATE TABLE IF NOT EXISTS `item_tbl` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(20) NOT NULL,
  `item_description` varchar(50) NOT NULL,
  `item_img` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `item_price` decimal(10,0) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `item_tbl`
--

INSERT INTO `item_tbl` (`item_id`, `item_name`, `item_description`, `item_img`, `qty`, `item_price`) VALUES
(1, 'coffe', 'test of bharuch', 'images/download.jpg', 8, '100'),
(2, 'coffe', 'test of bharuch', 'images/logo2.jpg', 1, '100');

-- --------------------------------------------------------

--
-- Table structure for table `order_tbl`
--

CREATE TABLE IF NOT EXISTS `order_tbl` (
  `order_id` int(10) NOT NULL,
  `user_email` varchar(20) NOT NULL,
  `item_id` int(10) NOT NULL,
  `Total_amount` float NOT NULL,
  `order_Date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tbl`
--


-- --------------------------------------------------------

--
-- Table structure for table `payment_tbl`
--

CREATE TABLE IF NOT EXISTS `payment_tbl` (
  `payment_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(20) NOT NULL,
  `card_no` decimal(10,0) NOT NULL,
  `card_name` varchar(20) NOT NULL,
  `exp` date NOT NULL,
  `valid` date NOT NULL,
  `cvv` int(3) NOT NULL,
  `total_amount` float NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `payment_tbl`
--


-- --------------------------------------------------------

--
-- Table structure for table `reg_tbl`
--

CREATE TABLE IF NOT EXISTS `reg_tbl` (
  `name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile` int(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg_tbl`
--

INSERT INTO `reg_tbl` (`name`, `email`, `password`, `mobile`, `address`) VALUES
('asjdf', 'kajsd', 'asfkj', 942653635, 'kjsaf'),
('qjwkhe', 'kjhw', 'kajfhs', 564, 'asf'),
('aksjdh', 'haskdjh', 'aksjhd', 654, 'asd'),
('rahul', 'rp240796@gmail.com', 'rahul', 2147483647, 'bharuch');
